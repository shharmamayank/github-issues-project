import React, { Component } from 'react'
import { Route, Link, Routes, Router } from "react-router-dom";
import "./issues.css"
import DetailsofIssues from './DetailsofIssues';

export class Issues extends Component {
    state = { issues: [], page: 1, isError: (false), isLoading: (true) }
    componentDidMount() {
        try {
            fetch('https://api.github.com/repos/rails/rails/issues').then(res => res.json()).then(res => this.setState({ issues: res, isLoading: false })).catch(error => {
                console.log("Error while fetching the data");
            })
        } catch (error) {

            this.setState({ isError: true, isLoading: false })
            console.log("error while fetchin the data")
        }
    }
    handleNextClick = async () => {
        try {
            let url = (`https://api.github.com/repos/rails/rails/issues?page=${this.state.page + 1}`);
            let data = await fetch(url);
            let parsedData = await data.json()
            this.setState({
                page: this.state.page + 1,
                issues: parsedData,

            })
        } catch (error) {
            console.log("Error while fetching the data for next button");
        }

    }
    handlePreviousClick = async () => {
        try {
            let url = `https://api.github.com/repos/rails/rails/issues?page=${this.state.page - 1}`;
            let data = await fetch(url);
            let parsedData = await data.json()
            this.setState({
                page: this.state.page - 1,
                issues: parsedData
            })
        } catch (error) {
            console.log("Error while fetching the data for previous button");
        }

    }
    render() {

        return this.state.isError ? (
            <h1>Error while Fetching the data</h1>
        ) : this.state.isLoading ? (
            <div className="loading-continer">Loading...</div>
        ) : (
            <div>
                <div className="heading-container">
                    <h1>Issues for rails/rails</h1>
                </div>
                {this.state.issues.length ? (
                    <>
                        {
                            this.state.issues.map(data => {
                                return (<div className='main-container'>
                                    <div>
                                        <img className='img-container' src={data.user.avatar_url} alt="" />
                                        <p>{data.user.login}</p>
                                    </div>
                                    <div className='main-issue-container'>
                                        <Link to={`/${data.number}`}>
                                            <h3 className='issue-text'><span className='number-span'>#{data.number}</span>{data.title}</h3>
                                        </Link>
                                        <p className='issue-text'>{data.body}</p>
                                    </div>
                                    <div> <hr /></div>
                                </div>
                                )

                            })
                        }
                        <div className='btn-class' >
                            <button disabled={this.state.page <= 1} type='button' onClick={this.handlePreviousClick}>&larr; Previous</button>
                            <button type='button' onClick={this.handleNextClick}> Next &rarr; </button>
                        </div>
                    </>
                ) : (
                    <div>
                        <h1>Go back to previous</h1>
                    </div>
                )}

                <Routes>
                    <Route exact path='/:issues' element={<DetailsofIssues page={this.state.page} />} />
                </Routes>

            </div>
        )
    }
}   

export default Issues
