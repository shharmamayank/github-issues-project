import React, { Component } from 'react'
import { Route, Link, Router } from "react-router-dom";
export class DetailsofIssues extends Component {
    state = {
        issues: [],
        isLoading: true,
        isError: false
    }
    async componentDidMount() {
        try {
            // console.log(this)
            const response = await fetch(
                `https://api.github.com/repos/rails/rails/issues?page=${this.props.page}`
            );
            const data = await response.json();
            const IssueId = window.location.pathname.slice(1);
            const issue = data.filter((issue) => issue.number == IssueId);
            this.setState({ issues: issue[0], isLoading: false });
        } catch (err) {
            this.setState({ isLoading: false, isError: true });
        }
    }
    render() {
        return this.state.isError ? (
            <h1>Error While Fetching the data</h1>
        ) : this.state.isLoading ? (
            <div>Loading...</div>
        ) : (
            <>
                <Link to="/">
                    <button>back</button>
                </Link>
                {
                    <>
                        <div >
                            <h1>{this.state.issues.title}</h1>
                        </div>
                        <div >{this.state.issues.number}</div>
                        <div >{this.state.issues.state}</div>
                        <div>
                            <img
                                src={this.state.issues.user.avatar_url}
                                alt="avatar"
                                className="user-avatar"
                            />
                            <div>{this.state.issues.user.login}</div>
                        </div>
                        <hr />
                        <div >{this.state.issues.body}</div>
                        <hr />
                    </>

                }
            </>
        );
    }

}


export default DetailsofIssues
